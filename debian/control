Source: python-certbot-dns-route53
Maintainer: Debian Let's Encrypt Team <team+letsencrypt@tracker.debian.org>
Uploaders: Harlan Lieberman-Berg <hlieberman@debian.org>,
           Andrew Starr-Bochicchio <asb@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-python,
               python3,
               python3-acme-abi-1,
               python3-boto3,
               python3-certbot-abi-1 (>= 1.1),
               python3-mock,
	       python3-pytest,
               python3-setuptools (>= 1.0),
               python3-sphinx (>= 1.3.1-1~),
               python3-sphinx-rtd-theme,
               python3-zope.interface
Standards-Version: 4.5.0
Homepage: https://certbot.eff.org/
Vcs-Git: https://salsa.debian.org/letsencrypt-team/certbot/certbot-dns-route53.git
Vcs-Browser: https://salsa.debian.org/letsencrypt-team/certbot/certbot-dns-route53
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-certbot-dns-route53
Architecture: all
Depends: certbot,
	 python3-certbot-abi-1 (>= 1.1),
         ${python3:Depends},
         ${misc:Depends}
Enhances: certbot
Description: Route53 DNS plugin for Certbot
 The objective of Certbot, Let's Encrypt, and the ACME (Automated
 Certificate Management Environment) protocol is to make it possible
 to set up an HTTPS server and have it automatically obtain a
 browser-trusted certificate, without any human intervention. This is
 accomplished by running a certificate management agent on the web
 server.
 .
 This agent is used to:
 .
   - Automatically prove to the Let's Encrypt CA that you control the website
   - Obtain a browser-trusted certificate and set it up on your web server
   - Keep track of when your certificate is going to expire, and renew it
   - Help you revoke the certificate if that ever becomes necessary.
 .
 This package contains the Route53 DNS plugin to the main application.

Package: python-certbot-dns-route53-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Description: Documentation for the Route53 DNS plugin for Certbot
 The objective of Certbot, Let's Encrypt, and the ACME (Automated
 Certificate Management Environment) protocol is to make it possible
 to set up an HTTPS server and have it automatically obtain a
 browser-trusted certificate, without any human intervention. This is
 accomplished by running a certificate management agent on the web
 server.
 .
 This package contains the documentation for the Route53 DNS plugin to
 the main application.
